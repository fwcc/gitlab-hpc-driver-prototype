#!/usr/bin/env bash

set -eo pipefail

BOLD_RED='\033[1;31m'
BOLD_GREEN='\033[1;32m'
NC='\033[0m'        # no color

# This is needed since the runner's working directory is /tmp which is not shared between nodes.
# TODO: Find out why the runner uses /tmp instead of its working directory, anyway.
WORK_DIR="/home/$USER"
SLURM_OPTIONS=(cpus-per-task nodes ntasks ntasks-per-node partition gres)

function getSlurmVar {
    local option
    option=$(echo "${2^^}" | tr - _)

    {
        sed -e 's/\\n/\n/g' "$1" | grep "export CI_SLURM_$option=" | cut -d "=" -f 2 | tr -d "$\\\'"
    } || {
        # TODO: return sensible default values?
        echo ""
    }
}

function tailFile {
    touch "$1"  # make sure the file exists
    #until [[ -f "$1" ]]; do sleep 0.1; done && tail -f "$1"
    tail -f "$1"
}

function pidChildOf {
    ps --ppid "$1" | tail -n +2 | head -n 1 | awk '{ print $1 }'
}

function slurmJobState {
    scontrol show job="$1" | grep "JobState" | awk '{ print $1 }' | cut -d "=" -f 2
}

function slurmJobExitCode {
    scontrol show job="$1" | grep "ExitCode" |
    awk '{ print $5 }' | cut -d "=" -f 2 | cut -d ":" -f 1
}

#
# ./this_script [my_args...] script stage_name
#                            \____/
#                       This has to be run!
#

module load git 2> /dev/null

script=${*: -2:1}
runStage=${*: -1:1}

# Only run the build script on the cluster.
if [[ "$1" == "hemera" && "$runStage" == "build_script" ]]; then
    # This will be the name of the batch script that is constructed from the script passed to this
    # program. Later we move this to the old script name.
    newScript="$script.new"

    {
        # write shebang and SBATCH options to new script
        echo "#!/usr/bin/env bash"
        echo "#SBATCH --workdir=$WORK_DIR"
        echo "#SBATCH --output=$WORK_DIR/slurm-%j.out"
        echo "#SBATCH --error=$WORK_DIR/slurm-%j.err"

        # iterate over SLURM options and get the desired setting
        # only write to new script if variable not empty
        for option in "${SLURM_OPTIONS[@]}"; do
            slurmVar=$(getSlurmVar "$script" "$option")
            if [[ -n "$slurmVar" ]]; then
                echo "#SBATCH --$option=$slurmVar"
            fi
            unset -v slurmVar
        done

        # write settings to new script
        # TODO: Better read out these settings than assume they'll always stay like this
        echo "set -eo pipefail"
        echo "set +o noclobber"
        echo ""

        # add the rest of the old script
        tail -n +5 "$script"
    } >> "$newScript"

    # move the script
    mv "$newScript" "$script"

    # pass the script to sbatch, catching its job ID
    chmod +x "$script"
    1>&2 echo -e "${BOLD_GREEN}Sending batch script to Slurm$NC"
    jobID=$(sbatch "$script" | awk '{ print $4 }')

    outFile="$WORK_DIR/slurm-$jobID.out"
    errFile="$WORK_DIR/slurm-$jobID.err"

    1>&2 echo -e "${BOLD_GREEN}Starting live output:$NC"

    # spawn subshells for live output of std and err
    (tailFile "$outFile") &
    pidTailOutParent="$!"
    (tailFile "$errFile") &
    pidTailErrParent="$!"

    # wait for job to finish
    #until [[ $(squeue -j "$jobID" | wc -l) -le "1" ]]; do sleep 2; done
    #1>&2 echo -e "${BOLD_GREEN}Job finished$NC"

    while true; do
        jobState=$(slurmJobState "$jobID")
        1>&2 echo "Slurm job state: $jobState"

        case "$jobState" in
            "BOOT_FAIL"|"CANCELLED"|"DEADLINE"|"FAILED"|"NODE_FAIL"|"OUT_OF_MEMORY"|"PREEMPTED"|"REVOKED"|"SPECIAL_EXIT"|"TIMEOUT")
                exitCode="$SYSTEM_FAILURE_EXIT_CODE"
                1>&2 echo -e "${BOLD_RED}Slurm job stopped$NC"
                break;;
            "COMPLETED")
                1>&2 echo -e "${BOLD_GREEN}Slurm job completed$NC"
                break;;
            "CONFIGURING"|"COMPLETING"|"PENDING"|"RUNNING"|"RESV_DEL_HOLD"|"REQUEUE_FED"|"REQUEUE_HOLD"|"REQUEUED"|"RESIZING"|"SIGNALING"|"STAGE_OUT"|"STOPPED"|"SUSPENDED")
                ;;
        esac

        sleep 5
    done

    pidTailOut=$(pidChildOf "$pidTailOutParent")
    pidTailErr=$(pidChildOf "$pidTailErrParent")

    # send SIGPIPE to suppress output when killing
    kill -13 "$pidTailOut" "$pidTailErr"

    # cleanup
    rm "$outFile" "$errFile"

    # set exit code if still not set and pass it to GitLab CI on exit
    if [[ -z "$exitCode" ]]; then exitCode=$(slurmJobExitCode "$jobID"); fi
    exit "$exitCode"

else
    # run script, propagate non-zero exit code as build failure code for CI
    if "$script"; then
        exit 0
    else
        exit "$BUILD_FAILURE_EXIT_CODE"
    fi
fi

