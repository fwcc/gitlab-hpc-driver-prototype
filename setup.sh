#!/usr/bin/env bash

RUNNER_CONFIG_DIR="/home/$USER/.gitlab-runner"
REPO_DIR="$(pwd)"

which "gitlab-runner" > /dev/null

mkdir -p "$RUNNER_CONFIG_DIR"
cd "$RUNNER_CONFIG_DIR"
ln -s "$REPO_DIR/runner-config.toml.example" "config.toml"

