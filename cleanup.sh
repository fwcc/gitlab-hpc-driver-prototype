#!/usr/bin/env bash

set -e

source "/home/$USER/gitlab-runner-custom/base.sh"

rm -r "/home/$USER/builds/$CUSTOM_ENV_PATH"

