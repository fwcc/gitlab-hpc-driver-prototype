#!/usr/bin/env bash

source "/home/$USER/gitlab-runner-custom/base.sh"

cat << EOS
{
    "builds_dir": "/home/$USER/builds/$CUSTOM_ENV_PATH",
    "cache_dir": "/home/$USER/cache/$CUSTOM_ENV_PATH"
}
EOS
